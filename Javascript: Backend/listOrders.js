const Items = require("../data/orders.js");

module.exports = {
  method: 'get',
  path: '/orders',
  async handler(request, response) {
    const items = await Orders.getAll();
    response.status(200).json(orders);
  }
};
