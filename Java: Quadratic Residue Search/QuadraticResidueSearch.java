// Implementation of closing hashing with quadratic residue search.
// The probe sequence is defined by h(k), h(k) - i^2 1, h(k) + i^2, ...
// 1 <= i <= (tableSize - 1)/2 all modulus table size.

/**
 * Implementation of closing hashing with quadratic residue search.
 * The probe sequance is defined by h(k), h(k) - i^2 1, h(k) + i^2, ...
 * 1 <= i <= (tableSize - 1)/2 all modulus table size.
 *
 * @author (Andrew Sychtysz)
 * @version (12/6/2019)
 */

public class QuadraticResidueSearch <E> {
    private E[] dataTable;
    private Status[] statusTable;
    final private int TABLESIZE = 300151;

    @SuppressWarnings("unchecked")
    public QuadraticResidueSearch () {
      dataTable = (E[])new Object[TABLESIZE];
      statusTable = new Status[TABLESIZE];
      for (int i = 0; i < TABLESIZE; i++)
         statusTable[i] = Status.EMPTY;
    }

    private int myMod(int index) {
		while (index < 0)
			index += TABLESIZE;
		return index % TABLESIZE;
    }

    /**
     * @param item element to be inserted into the dictionary
     * @return number of collisions encountered during this insert
    */
    public int insert(E item) {
    	int location = item.hashCode() % TABLESIZE;
    	int collisionCount = 0;
    	int i = 0;
    	
    	while (statusTable[location] == Status.OCCUPIED) {
    		i++;
    		location = (int) (Math.pow(location + i, 2) % TABLESIZE);
    		collisionCount++;
    	}
    	statusTable[location] = Status.OCCUPIED;
    	dataTable[location] = item;
    	return collisionCount;
    } 
    /**
	 * @param item element to be searched in the dictionary
	 * @param count number of element-to-element comparisons made for this search
	 * @return the object if found; null otherwise
	 */
    public E search(E item, IntObject count) {
    	int location = item.hashCode() % TABLESIZE;
        int i = 0;
        while (statusTable[location] == Status.OCCUPIED ||  statusTable[location] == Status.DELETED) {
        	if (statusTable[location] == Status.OCCUPIED) {
        		count.setData(count.getData() +1);
        		if (dataTable[location].equals(item)) {
        			return dataTable[location];
        		}
        	}
    		i++;
    		location = (int) (Math.pow(location + i, 2) % TABLESIZE);
        	if (count.getData() > TABLESIZE) {
        		return null;
        	}
        }
        return null;
}  // search(...)

    /**
	 * @param item element to be deleted from the dictionary
	 * @param count number of element-to-element comparisons made for this deletion
	 * @return the object if found and deleted; null otherwise
	 */
    public E delete(E item, IntObject count) {
    	int location = item.hashCode() % TABLESIZE;
    	int i = 0; 
        while (statusTable[location] == Status.OCCUPIED ||  statusTable[location] == Status.DELETED) {
        	if (statusTable[location] == Status.OCCUPIED) {
        		count.setData(count.getData() +1);
        		if (dataTable[location].equals(item)) {
        			statusTable[location] = Status.DELETED;
        			return dataTable[location];
        		}
        	}
        	location = (int) (Math.pow(location + i, 2) % TABLESIZE);
        	if (count.getData() > TABLESIZE) {
        		return null;
        	}
        }
        return null;
    }
}