import java.util.List;

import java.util.ArrayList;

/*
 * This code represents a BinarySearchTree Data Structure.
 * @author Andrew Sychtysz
 * @Version  4.1 - BinarySearchTree.java
 * @Date 11/15/2019
 */ 

public class BinarySearchTree <E extends Comparable <E>>
        extends BinaryTree <E> implements SearchTree <E> {
   // this version uses iteration instead of recursion in search,
   // insert and delete

   // denotes whether the addition is successful
   protected boolean addReturn;

   // denotes whether the deletion is successful
   protected E deleteReturn;
   


   /**
     Constructor - initializes the tree to an empty tree
     */
   public BinarySearchTree () {
      super() ;
   }

   public BinarySearchTree(E data, BinarySearchTree <E> left,
          BinarySearchTree <E> right)  {
	   super(data, left, right);
   }

   /**
    * Searches for a given element in the binary search tree
    * @param someElement element to be searched
    * @return true if someElement is found in the tree; false
    * 		  otherwise.
    * 
    */
   // Complexity: O(h) - where h is the height of the tree.
   // In the worst case it could be O(n).  But on aveage
   // we can expect a complexity of O(log n)
   public boolean contains(E someElement) {
      return contains(someElement, root);
   }
   
   private boolean contains(E someElement, Node<E> target) {
	   if (target == null) {
		   return false;
	   }
	   int compResult = target.data.compareTo(someElement);
	   if (compResult == 0) {
		   return true;
	   }
	   if (compResult < 0) {
		   return contains(someElement, target.right);
	   }
	   return contains(someElement, target.left);
   }
   
   /**
    * Searches for a given element in the binary search tree
    * @param someElement element to be searched
    * @param count keeps track of the number of comparisons for this search
    * @return someElement
    */
   // Complexity: O(h) - where h is the height of the tree.
   // In the worst case it could be O(n). But on average,
   // We can expect a complexity of O(log n)
   public E find(E someElement, IntObject count) { 
	   Node<E> temp = root;
	   count.setData(0);
	   while (temp!= null) {
		   int compResult = someElement.compareTo(temp.data);
		   count.setData(count.getData() + 1);		   
		   if (compResult > 0) {
		   temp = temp.right;		   
		   } else if (compResult < 0) {
		   temp = temp.left;
		   }
		   else return temp.data;
	   }
	   return null;
   }

    /**
     * Inserts an element into a BST
     * @param someElement element that needs to be inserted
     * @return true if the inserstion is succesful, false otherwise
     * 
    */ 
   // Complexity: O(h) - where h is the height of the tree.
   // In the worst case it could be O(n).  But on average
   // we can expect a complexity of O(log n)
   public boolean add(E someElement) {
	 root = add(root, someElement);
     return addReturn;
   }
   
   /**
    * Recursive add method.
    * post: the data field addReturn is set true if the item is added to the tree,
    * 		false if the item is already in the tree.
    * @param localRoot the local root of the subtree
    * @param item The object to be inserted
    * @return The new local root that now contains the inserted item
   */  	
   private Node<E> add(Node<E> localRoot, E item) {
	   if (localRoot == null) {
		   // item is not in the tree - insert it.
		   addReturn = true;
		   return new Node<E>(item);
	   } else if (item.compareTo(localRoot.data) == 0) {
		   // item is equal to localRoot.data
		   addReturn = false;
		   return localRoot;
	   } else if (item.compareTo(localRoot.data) < 0) {
		   // item is less than localRoot.data
		   localRoot.left = add(localRoot.left, item);
		   return localRoot;
	   } else {
		   // item is greater than localRoot.data
		   localRoot.right = add(localRoot.right, item);
		   return localRoot;
	   }
   }
  
   
   /**
    * Removes an element from BST
    * @param someElement element that needs to be deleted
    * @return true if someElement is found in the tree and is
    * successfully deleted; returns false if someElement is not 
    * found in the tree.
    */
   // Complexity: O(h) - where h is the height of the tree.
   // In the worst case it could be O(n).  But on aveage
   // we can expect a complexity of O(log n)
   public boolean remove(E someElement) {
	   if (!contains(someElement)) {
		   return false;
	   }
	   root = remove(someElement, root);
	   return true;
   }
   
   /**
    * Recursive remove method.
    * post: the data field addReturn is set true if the item is added to the tree,
    * 		false if the item is already in the tree.
    * @param localRoot the local root of the subtree
    * @param target The object to be removed
    * @return The new local root that now contains the inserted item
   */  
   private Node<E> remove(E someElement, Node<E> target) {
	   if (target == null) {
		   return target;
	   }
	   int compareResult = target.data.compareTo(someElement);
	   if (compareResult < 0) {
		   target.right = remove(someElement, target.right);
	   } else if (compareResult > 0) {
		   target.left = remove(someElement, target.left);
	   } else {
		   if (target.right == null) {
			   return target.right;
		   } else if  (target.right == null) {
			   return target.left;
		   } else {
			   target.data = getMin(target.right);
			   target.right = remove(target.data, target.right);
		   }
	   }
	   return target;
   }
   
   /**
    * Private helper method to get the Minimum data value 
    * of the set.
    * @param target
    * @return
    */
   private E getMin(Node<E> target) {
	   E minTarget = target.data;
	   while (target.left != null) {
		   minTarget = target.left.data;
		   target = target.left;
	   }
	   return minTarget;
   }

   /** @return the minimum element in the Set */
   // Complexity: O(h) - where h is the height of the tree.
   // In the worst case it could be O(n).  But on aveage
   // we can expect a complexity of O(log n)
   public E first() {
	   return first(root);
   }
   
   /**
    * Recursive first() method.
    * @param minNode
    * @return E farthest left node.
    */   
   private E first(Node<E> minNode) {
	   if (minNode == null)
		   return null;
	   else if (minNode.left == null)
		   return minNode.data;
	   else
		   return first(minNode.left);
   }

   /** @return the maximum element in the Set */
   // Complexity: O(h) - where h is the height of the tree.
   // In the worst case it could be O(n).  But on aveage
   // we can expect a complexity of O(log n)
   public E last() {
	   return last(root);
   }
   
   private E last(Node<E> maxNode) {
	   if (maxNode == null)
		   return null;
	   else if (maxNode.right == null)
		   return maxNode.data;
	   else
		   return last(maxNode.right);
   }

   	/**
   	 * Starter method delete.
   	 * post: The object is not in the tree.
   	 * @param target The object to be deleted
   	 * @return The object deleted from the tree
   	 *         or null if the object was not in the tree
   	 */
   public E delete(E target) {
	   if (root == null)
		   return null;
	   if (root.left == null && root.right == null) {
		   E result = root.data;
		   root = null;
		   return result;
	   }
       root = delete(root, target);
       return root.data;
   }


   private Node<E> delete(Node<E> localRoot, E item) {
       if (localRoot == null) {
           // item is not in the tree.
           deleteReturn = null;
           return localRoot;
       }
       // Search for item to delete.
       int compResult = item.compareTo(localRoot.data);
       if (compResult < 0) {
           // item is smaller than localRoot.data.
           localRoot.left = delete(localRoot.left, item);
           return localRoot;
       } else if (compResult > 0) {
           // item is larger than localRoot.data.
           localRoot.right = delete(localRoot.right, item);
           return localRoot;
       } else {
           // item is at local root.
           deleteReturn = localRoot.data;
           if (localRoot.left == null) {
               // If there is no left child, return right child
               // which can also be null.
               return localRoot.right;
           } else if (localRoot.right == null) {
               // If there is no right child, return left child.
               return localRoot.left;
           } else {
               // Node being deleted has 2 children, replace the data
               // with inorder predecessor.
               if (localRoot.left.right == null) {
                   // The left child has no right child.
                   // Replace the data with the data in the
                   // left child.
                   localRoot.data = localRoot.left.data;
                   // Replace the left child with its left child.
                   localRoot.left = localRoot.left.left;
                   return localRoot;
               } else {
                   // Search for the inorder predecessor (ip) and
                   // replace deleted node's data with ip.
                   localRoot.data = find((E) localRoot.left, null);
                   return localRoot;
               }
           }
       }
   }	     
}