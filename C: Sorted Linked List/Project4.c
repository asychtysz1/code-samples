/* C program that performs various operations on a Sorted Linked List that holds
the records of students as struct data type. (Hint:  This program will be a combination
programs done as Lab13a and Lab13-Addendum.)
Date: 4/3/2020
Author: Andrew Sychtysz */
#include<stdio.h>
#include<stdlib.h>
struct date
{
	int day, month, year;
};

struct student
{
    int studentId;
    char studentName[30];
    float gpa;
    struct date dt_of_reg;
    struct student *next;
};

struct student *start;
void insertStudentNode();
void printStudentNodes();
void countStudentNodes();
void deleteStudentNode();
void modifyStudentNode();
struct student *getStudentNode();

int main()
{
    start = NULL; // Empty Linked List
    int choice = 0;

    do
    {
        fflush(stdin);
        printf("\nOperations on Linked List");
        printf("\n***************************************");
        printf("\n1. Insert a New Student Record");
        printf("\n2. Print Student Records");
        printf("\n3. Delete a Student Record");
        printf("\n4. Count Student Records");
        printf("\n5. Modify a Student Record");
        printf("\n6. Quit");
        printf("\n Enter Choice (1-6): ");
        scanf("%d", &choice);
        fflush(stdin);
        printf("\n");

        switch(choice)
        {
            case 1: insertStudentNode();
                break;
            case 2: printStudentNodes();
                break;
            case 3: deleteStudentNode();
                break;
            case 4: countStudentNodes();
                break;
            case 5: modifyStudentNode();
                break;
            case 6: break;
        }
    } while (choice != 6);
    return 0;
}

/* Gets the data for the node from the user */
struct student *getStudentNode()
{
    struct student *new;
    new = (struct student*)malloc(sizeof(struct student));
    if (new == NULL)
        printf("\nMemory Error!!\n");

        printf("Enter Student's ID:\n");
        scanf("%d", &new->studentId);
        fflush(stdin);
        printf("Enter Student's name:\n");
        scanf("%[^\t\n]s", new->studentName);
        fflush(stdin);
        printf("Enter Student's gpa:\n");
        scanf("%f", &new->gpa);
        fflush(stdin);
        printf("Enter Student's Date of Registration (mm/dd/yyyy):\n");
        scanf("%d/%d/%d",&new->dt_of_reg.month, &new->dt_of_reg.day, &new->dt_of_reg.year);
        fflush(stdin);
    new -> next = NULL;
    return(new);
}

/*Adds a new node in the sorted Linked List in ascending order*/
void insertStudentNode()
{
    struct student *getStudentNode(); // Private function declaration

    struct student *ptr, *new, *prev;

    new = getStudentNode();

    if (start == NULL) // Linked list was initially empty and so the new node is now the first node
    {
        start = new;
        return;
    }

    // Traverse the list and insert new node in correct sorted place
    for (ptr = start; (ptr)&&((new->studentId)>(ptr->studentId)); prev = ptr, ptr = ptr -> next);

    if (ptr!=start) //new node is not the first node, but somewhere in the middle or end
    {
        prev -> next = new;
        new -> next = ptr;
    }
    else //new node is the first node in an non-empty linked list
    {
        new -> next = start;
        start = new;
    }
    return;
}

/*Deletes a node from the list*/
void deleteStudentNode()
{
    struct student *prev, *currPtr;
    if (start == NULL)
        printf("\n List does not contain data.\n");
    else
    {
        int key;
        printf("\nPlease enter the value you wish to delete(Only first occurrence will be deleted): ");
        scanf("%d", &key);
        fflush(stdin);
        //Traverse the list and keep comparing the values
        for(currPtr = start; (currPtr)&&((currPtr->studentId) != key); prev = currPtr, currPtr = currPtr -> next);
        if (currPtr == NULL)
            printf("\nStudent Record not found");
        else
        {
            if(currPtr == start) //node to be deleted is the first node
            {
                start = currPtr -> next;
                free(currPtr);
            }        for (ptr = start; ptr; ptr = ptr -> next) {
        printf(" Student id: %d\n", ptr->studentId);
        printf(" Student Name: %s \n", ptr->studentName);
        printf(" Student gpa: %0.2f\n", ptr->gpa);
        printf(" Student's Date of Registrations: %d/%d/%d\n\n", ptr->dt_of_reg.month, ptr->dt_of_reg.day, ptr->dt_of_reg.year);
        }
            else
            {   // Node to be deleted is somewhere in the middle or at the end of the list
                prev -> next = currPtr -> next;
                free(currPtr);
            }
        }
    }
}

/*Prints the contents of the linked list*/
void printStudentNodes()
{
    struct student *ptr;
    if(start == NULL)
        printf("\n List does not contain data. \n");
    else
        /*Traverse the entire linked List*/
        for (ptr = start; ptr; ptr = ptr -> next) {
        printf(" Student id: %d\n", ptr->studentId);
        printf(" Student Name: %s \n", ptr->studentName);
        printf(" Student gpa: %0.2f\n", ptr->gpa);
        printf(" Student's Date of Registrations: %d/%d/%d\n\n", ptr->dt_of_reg.month, ptr->dt_of_reg.day, ptr->dt_of_reg.year);
        }
    printf("\n");
}

/*Function that counts the amount of nodes in the list*/
void countStudentNodes()
{
    int count = 0;
    struct student *ptr;

    if(start == NULL)
        printf("\n List does not contain data. \n");
    else
    {
        /* Traverse the entire linked list */
        for(ptr = start; ptr; ptr = ptr -> next, count++);
        printf("\n This linked list has %d node(s). \n\n", count);
        printf("\n");
    }
}

void modifyStudentNode()
{
    struct student *prev, *currPtr;
    if (start == NULL)
        printf("\n List does not contain data.\n");
    else
    {
        int key;
        printf("\nPlease enter the student ID number you wish to modify: ");
        scanf("%d", &key);
        fflush(stdin);
        //Traverse the list and keep comparing the values
        for(currPtr = start; (currPtr)&&((currPtr->studentId) != key); prev = currPtr, currPtr = currPtr -> next);
        if (currPtr == NULL)
            printf("\nStudent ID is not found.\n");
        else
        {
        printf("--------------Student Record you are Modifying--------------\n");
        printf(" Student id: %d\n", currPtr->studentId);
        printf(" Student Name: %s \n", currPtr->studentName);
        printf(" Student gpa: %0.2f\n", currPtr->gpa);
        printf(" Student's Date of Registrations: %d/%d/%d\n\n", currPtr->dt_of_reg.month, currPtr->dt_of_reg.day, currPtr->dt_of_reg.year);
        printf("------------------Please Begin Modifying---------------------\n");
        printf("Enter Student's name:\n");
        scanf("%[^\t\n]s", currPtr->studentName);
        fflush(stdin);
        printf("Enter Student's gpa:\n");
        scanf("%f", &currPtr->gpa);
        fflush(stdin);
        printf("Enter Student's Date of Registration (mm/dd/yyyy):\n");
        scanf("%d/%d/%d",&currPtr->dt_of_reg.month, &currPtr->dt_of_reg.day, &currPtr->dt_of_reg.year);
        printf("------You have Successfully modified Student ID #%d ---------\n", currPtr->studentId);
        }
    }
}

